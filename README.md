##RimeIME Portable

Rime Portable, a portable version of Rime Input.

You should put your personal data into "usr" folder and the Rime (小狼毫) application into "weasel" folder. Run "install.bat" to install the portable version to your computer and "uninstall.bat" to uninstall from your computer.

批處理實現Rime小狼毫輸入便攜版

批處理作者：**大水牛** 2013.9.11

<img width="650" src="img/folder-structure.jpg">

Cangjie-5 sequence for traditional and simplified Chinese characters, more than 70,000 characters in total.

個人碼表：倉頡五代

- 2015.06.29  調整“|”和“/jt”
- 2015.06.23  Ctrl鍵切換中英文

[rime輸入法項目](https://github.com/rime/home/wiki)

[倉頡輸入法相關書籍](book/)

| | |
| :--- | :--- |
| 倉頡敎程 | 一天学会仓颉输入法.pdf |
| | 第五代倉頡輸入法手冊.pdf |
| | 仓颉五代“非韭”等字拆法.docx |
| | 在哪裡切--仓颉.7z |
| | …… |
| 正體字敎程 | 繁體字通俗演義(2010.09.23).pdf |
| | 國字標準字體楷書母稿＜教育部字序＞.pdf |
| | …… |
